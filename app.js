import "./x-talk.js";

import { data } from "./data.js";

var applyVisibility = ( entry ) => {
	let talk = entry.target;

	talk.setAttribute( "visible", entry.isIntersecting ? true : false );
}
 var intersection = new IntersectionObserver( ( entries ) => entries.forEach( applyVisibility ), {
	"threshold": .75
 } );

var nodes = data.map( ( entry ) => {
	let talk = document.createElement( "x-talk" );

	talk.setAttribute( "title", entry.title );
	talk.setAttribute( "video", entry.video );
	talk.setAttribute( "slides", entry.slides );
	talk.setAttribute( "type", entry.type );
	talk.setAttribute( "visible", false );
	talk.setSpeakers( entry.speakers );
	talk.setIntersection( intersection );

	return talk;
} );

nodes.forEach( ( node ) => document.body.appendChild( node ) );