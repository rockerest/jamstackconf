export var data = [
	{
		"speakers": [ "Phil Hawksworth" ],
		"type": "main",
		"title": "What is the JAMstack?",
		"video": "Opye_qcRdUo",
		"slides": ""
	},
	{
		"speakers": [ "Matt Biilmann" ],
		"type": "agency",
		"title": "How We Talk About the JAMstack",
		"video": "VzQ0d8-nMhw",
		"slides": ""
	},
	{
		"speakers": [ "Thomas Reynolds" ],
		"type": "agency",
		"title": "The 3 Types of Clients You'll Need to Convince",
		"video": "mX4sRTNehVQ",
		"slides": ""
	},
	{
		"speakers": [ "Tim Fogarty" ],
		"type": "agency",
		"title": "Finding the Best Home for Your Data",
		"video": "i1qlu6pj23g",
		"slides": ""
	},
	{
		"speakers": [ "Taylor Gilbert" ],
		"type": "agency",
		"title": "3 Truths That You Can Bet The Next 5 Years On",
		"video": "HSSLI0jcMm0",
		"slides": ""
	},
	{
		"speakers": [ "Matt Biilmann", "Taylor Gilbert", "Thomas Reynolds", "Chris Bach" ],
		"type": "agency",
		"title": "Round Table Discussion",
		"video": "Or4NmGbyQeY",
		"slides": ""
	},
	{
		"speakers": [ "Zoltan Olah" ],
		"type": "lightning",
		"title": "Turning Design into Code the Painless Way",
		"video": "lK1gZUsTZ5w",
		"slides": ""
	},
	{
		"speakers": [ "Eric Gardner" ],
		"type": "lightning",
		"title": "Building Books From Static Sites",
		"video": "-2hOesSVoRw",
		"slides": ""
	},
	{
		"speakers": [ "Tanmai Gopal" ],
		"type": "lightning",
		"title": "How to Setup Production",
		"video": "CWHek7Cwr7Y",
		"slides": ""
	},
	{
		"speakers": [ "Bud Parr" ],
		"type": "lightning",
		"title": "Go Go Hugo!",
		"video": "SIUYsPnhGcM",
		"slides": ""
	},
	{
		"speakers": [ "Steve Gardner" ],
		"type": "lightning",
		"title": "Make More Pointless Things",
		"video": "q1qSxmfMIcI",
		"slides": ""
	},
	{
		"speakers": [ "Matt Biilmann" ],
		"type": "main",
		"title": "Keynote",
		"video": "mleYzTGzVc4",
		"slides": ""
	},
	{
		"speakers": [ "Monica Dinculescu" ],
		"type": "main",
		"title": "Bet You Didn't Think Your Browser Could Do That",
		"video": "f5QYSdpMs6Y",
		"slides": ""
	},
	{
		"speakers": [ "Prateek Bhatnagar" ],
		"type": "main",
		"title": "Supercharged PWAs With Preact CLI",
		"video": "-KVm3tRlqg4",
		"slides": ""
	},
	{
		"speakers": [ "Jessica Lord" ],
		"type": "main",
		"title": "Everyone is a Developer",
		"video": "x9a6poSXJIA",
		"slides": ""
	},
	{
		"speakers": [ "Chris Coyier" ],
		"type": "main",
		"title": "The All Powerful Front End Developer",
		"video": "grSxHfGoaeg",
		"slides": ""
	},
	{
		"speakers": [ "Simona Cotin" ],
		"type": "main",
		"title": "Build Scalable APIs Using GraphQL and Serverless",
		"video": "GTrHSQbxR-Y",
		"slides": ""
	},
	{
		"speakers": [ "Quincy Larson" ],
		"type": "main",
		"title": "How freeCodeCamp Serves Millions of Learners Using the JAMstack",
		"video": "NVRsYrUl6Cg",
		"slides": ""
	},
	{
		"speakers": [ "Scott Tolinski", "Wes Bos" ],
		"type": "main",
		"title": "Syntax FM Live!",
		"video": "L3UokgyCr4A",
		"slides": ""
	}
]