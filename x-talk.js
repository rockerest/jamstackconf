import "./x-speaker.js";
import "./youtube-embed.js";

var talkTypes = {
	"agency": "Agency Day",
	"lightning": "Lightning Talk",
	"main": "Conference Day"
}

var dataHandlers = {
	"title": ( dom, val ) => dom.querySelector( "h2" ).textContent = val,
	"video": ( dom, val, component ) => {
		let video = dom.querySelector( "youtube-embed" );
		let visible = component.getAttribute( "visible" );
		
		showVideo( video, val, visible == "true" );
	},
	"visible": ( dom, val, component ) => {
		let video = dom.querySelector( "youtube-embed" );

		showVideo( video, component.getAttribute( "video" ), val == "true" );
	},
	"type": ( dom, val ) => {
		let type = talkTypes[ val ] || val;

		dom.querySelector( ".type" ).textContent = type;
	}
}

function showVideo( video, url, visible ){
	var src = video.shadowRoot.querySelector( "iframe" ).getAttribute( "src" );

	if( visible && !src ){
		video.setAttribute( "video", url );
	}
}

class Talk extends HTMLElement{
	static get observedAttributes(){
		return [
			"title",
			"type",
			"video",
			"slides",
			"visible"
		]
	}
	constructor( ...args ){
		var self = super( ...args );
		var template = document.importNode( document.getElementById( "talk" ).content, true );

		self.attachShadow( { "mode": "open" } );

		self.shadowRoot.appendChild( template );
	}

	attributeChangedCallback( attr, oldAttr, newAttr ){
		if( dataHandlers[ attr ] ){
			dataHandlers[ attr ]( this.shadowRoot, newAttr, this );
		}
	}

	setSpeakers( speakers ){
		var speakersList = this.shadowRoot.querySelector( ".speakers" );

		var nodes = speakers.map( ( speaker ) => {
			let node = document.createElement( "x-speaker" );

			node.setAttribute( "name", speaker );

			return node;
		} );

		speakersList.innerHTML = "";
		nodes.forEach( ( speaker ) => speakersList.appendChild( speaker ) );
	}
	setIntersection( observer ){
		observer.observe( this );
	}
}

window.customElements.define( "x-talk", Talk );