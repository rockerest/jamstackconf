const YOUTUBE_ROOT = "https://www.youtube-nocookie.com/embed/";

var dataHandlers = {
	"video": ( dom, val ) => dom.querySelector( "iframe" ).setAttribute( "src", val )
};

class YoutubeEmbed extends HTMLElement{
	static get observedAttributes(){
		return [
			"video"
		]
	}
	constructor( ...args ){
		var self = super( ...args );
		var template = document.importNode( document.getElementById( "youtube-embed" ).content, true );

		self.attachShadow( { "mode": "open" } );

		self.shadowRoot.appendChild( template );
	}

	attributeChangedCallback( attr, oldAttr, newAttr ){
		if( dataHandlers[ attr ] ){
			dataHandlers[ attr ]( this.shadowRoot, YOUTUBE_ROOT + newAttr );
		}
	}
}

window.customElements.define( "youtube-embed", YoutubeEmbed );