var dataHandlers = {
	"name": ( dom, val ) => dom.querySelector( "span" ).textContent = val
}

class Speaker extends HTMLElement{
	static get observedAttributes(){
		return [
			"name"
		]
	}
	constructor( ...args ){
		var self = super( ...args );
		var template = document.importNode( document.getElementById( "speaker" ).content, true );

		self.attachShadow( { "mode": "open" } );

		self.shadowRoot.appendChild( template );
	}

	attributeChangedCallback( attr, oldAttr, newAttr ){
		if( dataHandlers[ attr ] ){
			dataHandlers[ attr ]( this.shadowRoot, newAttr );
		}
	}
}

window.customElements.define( "x-speaker", Speaker );